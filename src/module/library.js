import { ARSItem } from "./item/item.js";
import * as chatManager from "./chat.js";
import * as utilitiesManager from "./utilities.js";
import * as debug from "./debug.js";

export async function buildPackItemList() {
    console.log("Building pack items list for game.osric.library.packs.items", { game });
    const packItems = await utilitiesManager.getPackItems('Item', game.user.isGM);
    game.osric.library['packs'] = { items: packItems };
    console.log("Pack items list built as game.osric.library.packs.items", { game });
}
/**
 * 
 * This scans compendiums and world spells and places them into game.osric.library.packs.items.*
 * for use in various spell lists
 * 
 */
export default async function () {
    game.osric.library = {
        const: {
            location: {
                CARRIED: 'carried',
                EQUIPPED: 'equipped',
                NOCARRIED: 'nocarried',
            }
        }
    }

    buildPackItemList();
}

