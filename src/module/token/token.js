import * as debug from "../debug.js"
import * as utilitiesManager from "../utilities.js";
import * as effectManager from "../effect/effects.js";
import { ARS } from '../config.js';

/**
 * Token is a PlaceableObject and we need to manipulate certain views of things for it.
 */

export class ARSToken extends Token {

    get hasAura() {
        if (this.document.actor) {
            const auraEffects =
                this.document.actor.getActiveEffects().filter(effect => {
                    return effect.changes.some(changes => {
                        return changes.key.toLowerCase() === 'special.aura';
                    })
                });
            return auraEffects;
        }
        return undefined;
    }

    get isFriendly() {
        return (this.document.disposition == game.osric.const.TOKEN_DISPOSITIONS.FRIENDLY)
    }
    get isNeutral() {
        return (this.document.disposition == game.osric.const.TOKEN_DISPOSITIONS.NEUTRAL)
    }
    get isHostile() {
        return (this.document.disposition == game.osric.const.TOKEN_DISPOSITIONS.HOSTILE)
    }

    /** @override because we need to point to this.document.name not this.data.name for actor ID name views */
    _drawNameplate() {
        const style = this._getTextStyle();
        const name = new PreciseText(this.document.name, style);
        name.anchor.set(0.5, 0);
        name.position.set(this.w / 2, this.h + 2);
        return name;
    }

    // _onCreate(data, options, userId) {
    //     console.log("ARSToken _onCreate", { data, options, userId })
    //     super._onCreate(data, options, userId);
    // }

    _onUpdate(data, options, userId) {
        // console.log("ARSToken TokenAura _onUpdate", { data, options, userId }, this)
        super._onUpdate(data, options, userId);

        this.updateAuras();
    }

    /**@override */
    async draw() {
        // console.log("ARSToken TokenAura draw", { arguments }, this)
        const d = await super.draw(this, arguments);
        this.auraContainer = this.addChildAt(new PIXI.Container(), 0);
        this.updateAuras()
        return d;
    }

    updateAuras() {
        // console.log("ARSToken TokenAura _updateAuras 1 ===>", this)
        if (this.document?.flags?.osric?.aura) {
            const auraConfig = this.document.flags.osric.aura;
            if (auraConfig) {
                // this._checkAuras();
                if (this.auraContainer.removeChildren) {
                    this.auraContainer.removeChildren().forEach(a => a.destroy());
                }
                if (auraConfig.isEnabled) {
                    let hasPermission = false;
                    if (!auraConfig.permission || auraConfig.permission === 'all' ||
                        (auraConfig.permission === 'gm' && game.user.isGM)) {
                        hasPermission = true;
                    } else {
                        hasPermission = !!this.document?.actor?.testUserPermission(game.user, auraConfig.permission.toUpperCase());
                    }

                    // console.log("ARSToken TokenAura _updateAuras 2 ===>", { auraConfig, hasPermission })
                    if (hasPermission) {
                        const aura = new TokenAura(this, auraConfig.distance, auraConfig.color, auraConfig.isSquare, auraConfig.opacity);
                        this.auraContainer.addChild(aura);
                    }
                }
            } else {
                // console.log("ARSToken TokenAura _updateAuras NO AURACONFIG", this);
            }
        } else {
            // const tokenObj = this;
            // console.warn(`token.js updateAuras(): ${this.name} has no token.document?.flags?.osric?.aura =>`, { tokenObj })
            // populate defaults some defaults to avoid recreation issues
            this.document.setFlag("osric", "aura", { isEnabled: false, color: '#ffffff', opacity: 0.35, distance: 10, permission: 'gm', });
        }
        this.updateActiveEffectAura();
    }

    updateActiveEffectAura() {
        if (this.document.actor) {
            // get all the aura effects that are active
            const auraEffects =
                this.document.actor?.getActiveEffects().filter(effect => {
                    return effect.changes.some(changes => {
                        return changes.key.toLowerCase() === 'special.aura';
                    })
                });

            // console.log("token.js updateActiveEffectAura", { auraEffects });

            for (const effect of auraEffects) {
                const auraInfo = effectManager.getAuraInfo(effect, this.document);

                let hasPermission = false;
                if (!auraInfo.permission || auraInfo.permission === 'all' ||
                    (auraInfo.permission === 'gm' && game.user.isGM)) {
                    hasPermission = true;
                } else {
                    hasPermission = !!this.document?.actor?.testUserPermission(game.user, auraInfo.permission.toUpperCase());
                }

                // console.log("token.js updateActiveEffectAura", { distance, color, faction, isSquare, opacity })
                // if (auraInfo && (auraInfo.visibleAura || game.user.isGM)) {
                if (auraInfo && hasPermission) {
                    const aura = new TokenAura(this, auraInfo.distance, auraInfo.color, auraInfo.isSquare, auraInfo.opacity);
                    this.auraContainer.addChild(aura);
                }
            }
        } else {
            ui.notifications.error(`Token: ${this.name} does not have an associated actor.`);
        }
    }

}// end Token

/**
 * Skeleton test for token auras
 */
class TokenAura extends PIXI.Graphics {
    constructor(token, distance, color, isSquare = false, opacity = 0.25) {
        super();

        // console.log("TokenAura constructor", { token, distance, color, isSquare, opacity }, this)

        const squareGrid = canvas.scene.grid.type === 1;
        const dim = canvas.dimensions;
        const unit = dim.size / dim.distance;
        const [cx, cy] = [token.w / 2, token.h / 2];
        const { width, height } = token.document;

        if (distance <= 0) distance = 10;

        // console.log("TokenAura constructor", { squareGrid, dim, unit, cx, cy, width, height })
        let w, h;
        if (isSquare) {
            w = distance * 2 + (width * dim.distance);
            h = distance * 2 + (height * dim.distance);
        } else {
            [w, h] = [distance, distance];

            if (squareGrid) {
                w += width * dim.distance / 2;
                h += height * dim.distance / 2;
            } else {
                w += (width - 1) * dim.distance / 2;
                h += (height - 1) * dim.distance / 2;
            }
        }
        // set w/h
        w *= unit;
        h *= unit;

        this.filters = [this.#createReverseMaskFilter()];

        this.distance = distance;
        this.color = Color.from(color);
        // this.color = color;
        this.isSquare = isSquare;
        // this.source = 'aura';

        this.lineStyle(3, Color.from(color));
        this.beginFill(Color.from(color), opacity);
        if (isSquare) {
            const [x, y] = [cx - w / 2, cy - h / 2];
            this.drawRect(x, y, w, h);
        } else {
            this.drawEllipse(cx, cy, w, h);
        }
        this.endFill();
    }

    // updatePosition(x, y) {
    //     console.log("TokenAura updatePosition", { x, y }, this)
    //     this.position.set(x, y);
    // }

    /**
    * Create the reverse mask filter.
    * @returns {ReverseMaskFilter}  The reference to the reverse mask.
     */
    #createReverseMaskFilter() {
        if (!this.reverseMaskfilter) {
            this.reverseMaskfilter = ReverseMaskFilter.create({
                uMaskSampler: canvas.primary.tokensRenderTexture,
                channel: "a"
            });
            this.reverseMaskfilter.blendMode = PIXI.BLEND_MODES.NORMAL;
        }
        return this.reverseMaskfilter;
    }
}

/**
 * 
 * Extending tokenDocument so that we can filter the TokenConfig
 * dropdown list and block recursion issues
 * 
 * and also override name to hide npc names everywhere
 * 
 */
export class ARSTokenDocument extends TokenDocument {

    get nameRaw() {
        return this.name;
    }

    /**
     * 
     * @returns name processed with some tests
     */
    getName() {
        // console.log("getName() ARSTokenDocument", this)
        if (['npc', 'lootable'].includes(this?.actor?.type)) {
            if (!game.user.isGM) {
                // actor already does all the revelvant checks/points to alias
                return this.actor.getName();
            }
        }
        return this.name;
    }

    prepareDerivedData() {
        //     // for v10 requirements
        //     // console.log("token.js prepareDerivedData", this)
        super.prepareDerivedData();
        this.name = this.getName();
    }

    /**@override */
    static getTrackedAttributes(data, _path = []) {
        // console.log("getTrackedAttributes START", { data, _path });

        if (!data) {
            data = {};
            for (let model of Object.values(game.system.model.Actor)) {
                foundry.utils.mergeObject(data, model);
            }
        }

        /**
         * we need to filter the data of getTrackedAttributes()
         * otherwise it gets in a recursion loop and have stack error
         * 
         * we NEED to ignore "contains" and "containedIn" but the others are simply
         * to reduce the spam in the resources drop down for Token config
         * 
         */
        // const checkThese = ['attributes', 'abilities', 'details', 'power'];
        const ignoreThese = ['contains', 'containedIn', 'config', 'matrix', 'classes', 'inventory', 'memorizations', 'spellInfo', 'actionInventory', 'gear', 'containers', 'skills', 'weapons', 'spells', , 'races', 'rank', 'proficiencies', 'activeClasses', 'armorClass', 'abilityList', 'itemActionCount', 'mods', 'actionCount'];

        let reducedData = duplicate(data);
        // delete entries we ignore
        ignoreThese.forEach(entry => { delete reducedData[entry] })
        // console.log("getTrackedAttributes", { reducedData });        
        // cleaned up data, now send to the super.
        return super.getTrackedAttributes(reducedData, _path);
    }

    /**
     * Prepare token with light effects or default for NPC/PC
     */
    async updateLight() {
        // Check if the current user is a game master and if the lighting automation is enabled
        if (game.user.isGM && game.osric.config.settings.automateLighting) {
            const token = this;

            const light = {
                dim: 0,
                bright: 0,
                angle: 0,
                luminosity: token.light.luminosity,
                animation: {
                    type: 0,
                }
            };

            // Loop through each active effect on the token's actor
            for (const effect of token.actor.getActiveEffects()) {
                // Loop through each change in the effect
                for (const change of effect.changes) {
                    // If the change key is special.light and has a value
                    if (change.key === "special.light" && change.value.length) {
                        try {
                            const details = JSON.parse(change.value.toLowerCase());
                            const color = ARS.htmlBasicColors[details.color] ?
                                ARS.htmlBasicColors[details.color] : details.color;
                            const dim = parseInt(details.dim) || 0;
                            const bright = parseInt(details.bright) || dim;
                            const angle = parseInt(details.angle) || 360;
                            const animationType = details.animation || null;
                            const alpha = parseFloat(details.alpha) || null;

                            // Update light object properties if the conditions are met
                            if (light.dim < dim || (light.dim === 0 && dim)) {
                                light.dim = dim;
                                light.bright = bright;
                                light.angle = angle;
                                light.color = color;
                                light.animation.type = animationType;
                                light.alpha = alpha;
                            }
                        } catch (err) {
                            // Show an error notification if the light data field is formatted incorrectly
                            ui.notifications.error(`[${err}] LIGHT data field "${change.value}" is formatted incorrectly. It must be at least "{color: #htmlcolor, dim: lightrange}".`);
                        }
                    }
                }
            }

            // Try to update the token's light properties
            try {
                await token.update({ light });
            } catch (err) {
                // Log a warning if the token update for light fails
                console.warn(`${token.name}'s token update for light failed.`);
            }
        }
    }

    /**
    * Update token vision based on effects applied
    */
    async updateVision() {
        // If the user is not the game master, exit early
        if (game.user.isGM && game.osric.config.settings.automateVision) {

            // Get the token object from the current context
            const token = this;
            // Initialize default vision settings
            const sight = {
                enabled: false,
                range: 0,
                sightAngle: 360,
                visionMode: 'basic',
            };

            // Iterate through active effects on the token's actor
            for (const effect of token.actor.getActiveEffects()) {
                // Iterate through effect changes
                for (const change of effect.changes) {
                    // Check if the change affects vision and has a value
                    if (change.key === "special.vision" && change.value.length) {
                        try {
                            // Parse change value as JSON
                            const details = JSON.parse(change.value.toLowerCase());
                            const range = parseInt(details.range);
                            const sightAngle = parseInt(details.angle) || 360;
                            const visionMode = details.mode || 'basic';

                            // Update vision settings if the new range is greater
                            if (sight.range < range) {
                                sight.enabled = true;
                                sight.range = range;
                                sight.sightAngle = sightAngle;
                                sight.visionMode = visionMode;
                            }
                        } catch (err) {
                            // Show error notification if the change value is not formatted correctly
                            ui.notifications.error(`VISION data field "${change.value}" is formatted incorrectly. It must be at least "sightRange#"`);
                        }
                    }
                }
            }

            // If no vision changes were found in the active effects
            if (!sight.enabled) {
                // Helper function to set vision settings from the actor's prototype token
                function setSight(actor) {
                    sight.enabled = actor.prototypeToken.sight.enabled;
                    sight.range = actor.prototypeToken.sight.range;
                    sight.sightAngle = actor.prototypeToken.sight.angle;
                    sight.visionMode = actor.prototypeToken.sight.visionMode;
                }

                let visionRange = 0;

                // Handle NPC vision settings
                if (token.actor.type === 'npc') {
                    const visionCheck = `${token.actor.system?.specialDefenses} ${token.actor.system?.specialAttacks}`;
                    const [match, ...remain] = visionCheck.match(/vision (\d+)/i) || [];
                    visionRange = match ? parseInt(remain[0]) : 0;

                    if (visionRange) {
                        sight.enabled = true;
                        sight.range = visionRange;
                    } else {
                        setSight(token.actor);
                    }
                    // Handle character vision settings
                } else if (token.actor.type === 'character') {
                    setSight(token.actor);
                }
            }

            // Check if the actor has the 'blind' status effect
            const blind = (token.actor.effects.find(e => e.getFlag("core", "statusId") === 'blind'));

            // If the actor is not blind, update the token's vision settings
            if (!blind) {
                try {
                    await token.update({ sight });
                } catch (err) {
                    console.warn(`${token.name}'s token update for vision failed.`);
                }
            }

        }
    }


    /**
     * //BUG:
     * This doesn't work perfectly with tokens that are not scale/size 1:1. Anything above/below and it skews
     * some distances but I'm not sure why.
     * 
     * Get the distance to a token
     * 
     * @param {TokenDocument} tokenDocument 
     * @returns {Number} The distance between the tokens
     * 
     */
    getDistance(tokenDocument = undefined) {
        const sourceDocument = this;
        // console.log("token.js getDistance", { sourceDocument, tokenDocument })
        if (!tokenDocument) {
            ui.notifications.error(`token.js getDistance() requires tokenDocument target [${tokenDocument}]`)
            return {}
        }

        const sourceToken = sourceDocument.object;
        const targetToken = tokenDocument.object;

        // gridsize (5/10/etc)
        const gridDistance = canvas.scene.grid.distance;
        // pixels per grid (50/100/200/etc)
        const gridSize = canvas.scene.grid.size;
        // types of grid unit, (ft/mile/yard/etc)
        const gridUnits = canvas.scene.grid.units;

        // // figure out the size of the tokens so we can account for it's space
        const sourceDisplacement = (Math.max(sourceDocument.height, sourceDocument.width) / 2);
        const targetDisplacement = (Math.max(tokenDocument.height, tokenDocument.width) / 2);

        const tokenDisplacement = sourceDisplacement + targetDisplacement;
        let tokenSpace = tokenDisplacement * gridSize;

        const ray = new Ray(sourceToken.center, targetToken.center);
        // const ray = new Ray(sourceCenter, targetCenter);

        let nx = Math.abs(ray.dx);
        let ny = Math.abs(ray.dy);

        // Determine the number of straight and diagonal moves
        let nd = Math.min(nx, ny);
        let ns = Math.abs(ny - nx);
        // let lineDistance = (nd + ns);
        let rayDistance = Math.abs(ray.dx);
        // let rayDistance = (nd + ns);
        let range = rayDistance > 0 ? rayDistance - tokenSpace : 0;
        let tokenReduction = tokenSpace;
        // angles are slightly different, so if nx is < than ny, we adjust
        if (nd < ny) {
            ns = Math.abs(ny - nd);
            tokenReduction = tokenSpace - nx;
            range = ns - tokenReduction;
        }

        // const rayDistance = Math.abs(ray.dx);
        const between = range > 0 ? Math.floor((range / gridSize) * gridDistance) : 0;
        if (game.osric?.config?.settings?.debugMode)
            console.log(`Distance between ${sourceToken.name} and ${targetToken.name} is ${between} ${gridUnits}`);
        return between;
    }

    /**
     * Check for aura on token and then any token around it if it needs to apply
     * any changes.* entries on an effect that starts with aura.* will be applied
     * to relevant faction it affects.
     */
    async checkForAuras() {
        if (game.osric?.config?.settings?.debugMode)
            console.log("token.js checkForAuras START->", this.name)


        for (const tokenObj of canvas.tokens?.placeables) {
            const token = tokenObj.document;
            const allEffects = token.actor.getActiveEffects();
            const auraEffects =
                allEffects.filter(effect => {
                    return effect.changes.some(changes => {
                        return changes.key.toLowerCase() === 'special.aura';
                    })
                });

            // console.log("token.js checkForAuras", { token, actor, allEffects, auraEffects });
            // canvas.tokens?.placeables.forEach(t => {
            for (const t of canvas.tokens?.placeables) {
                // console.log("token.js checkForAuras", t.document.name, { token, t })
                for (const effect of auraEffects) {
                    await this.auraManagement(token, t, effect);
                }

                // now check to clean any effects left over from deletion/modifications to aura effect
                await t.document.checkForDanglingAuraEffects();
            }; // canvas

        }


    }

    async auraManagement(token, t, effect) {
        const sourceAuraUUID = effect.uuid;
        const auraInfo = effectManager.getAuraInfo(effect);
        // console.log("token.js auraManagement", { effect, auraInfo })
        if (auraInfo) {
            // if token.object.center doesn't exist, the token is probably being/deleted
            const tokenDistance = token.object?.center ? token.getDistance(t.document) : Infinity;
            // console.log("token.js auraManagement", { tokenDistance })

            // check that token disposition matches what aura is suppose to effect and they are in the aura 
            if ((auraInfo.faction === 'all' || t.document.disposition === auraInfo.disposition) && tokenDistance < auraInfo.distance) {
                if (game.osric?.config?.settings?.debugMode)
                    console.log(`token.js auraManagement) ${t.document.name} is within ${token.name}'s aura ${tokenDistance}<=${auraInfo.distance} for effect ${effect.label}`, { token, t, effect, auraInfo })
                //Check that effect isnt applied already
                if (!t.document.actor.effects.some(e => { return (e.getFlag("osric", "sourceAuraUUID") === sourceAuraUUID) })) {

                    // get any effect that starts with ^aura.* so we can apply them for this aura
                    // strip out ^aura. and leave the rest
                    const auraChanges = effect.changes
                        .filter(c => c.key.toLowerCase().startsWith("aura."))
                        .map(oC => {
                            const aC = duplicate(oC);
                            aC.key = aC.key.toLowerCase()?.replace(/^aura\./, "");
                            return aC;
                        });

                    // create effect with relevant "changes" entries.
                    const auraEffects = await t.document.actor.createEmbeddedDocuments("ActiveEffect", [{
                        flags: {
                            core: {},
                            osric: {
                                sourceAuraUUID: sourceAuraUUID,
                            }
                        },
                        label: `AURA:${effect.label}`,
                        icon: effect.icon,
                        origin: `${token.actor.uuid}`,
                        "duration.startTime": game.time.worldTime,
                        transfer: false,
                        "changes": auraChanges,
                    }], { parent: t.document.actor });
                } else {
                    // token already had the effect
                }

            } else {
                // token out of aura, remove effects that were applied
                const matchingIds = t.document.actor.effects.filter(e => { return e.getFlag("osric", "sourceAuraUUID") === sourceAuraUUID }).map(e => e.id);
                // console.log(`token.js checkForAuras matchingIds for ${t.document.name}`, { matchingIds })
                if (matchingIds.length) {
                    console.log("token.js auraManagement out of AURA REMOVE", { matchingIds })
                    await t.document.actor.deleteEmbeddedDocuments("ActiveEffect", matchingIds);
                }
            }
        }
    }

    /**
     * Check for any effects applied from a Aura that are no longer active (happens on delete effect/actor/token)
     * @param {*} t 
     */
    async checkForDanglingAuraEffects() {
        const auraAppliedEffects = this.actor.effects.filter(e => { return e.getFlag("osric", "sourceAuraUUID") });
        // console.log(`token.js checkForDanglingAuraEffects`, this.name, { auraAppliedEffects })
        if (auraAppliedEffects.length) {
            // console.log("token.js checkForDanglingAuraEffects", { auraAppliedEffects })
            for (const ae of auraAppliedEffects) {
                // console.log("token.js checkForDanglingAuraEffects", { ae }, ae.flags.osric.sourceAuraUUID)
                // if (ae.flags.osric.sourceAuraUUID) {
                const effectFromUUID = await fromUuid(ae.flags.osric.sourceAuraUUID)
                // console.log("token.js checkForDanglingAuraEffects", this.name, { effectFromUUID })
                if (!effectFromUUID || (effectFromUUID.disabled || effectFromUUID.isSuppressed)) {
                    // console.log(`token.js checkForDanglingAuraEffects removing AE`, this.name, { ae }, ae.uuid)
                    await this.actor.deleteEmbeddedDocuments("ActiveEffect", [ae.id]).catch(err => console.log(`Cleanup of ${ae.id}: ${err}`));
                } else if (effectFromUUID) {
                    const object = effectFromUUID.parent;
                    if (['npc', 'character'].includes(object.type)) {
                        const token = object.getToken();
                        if (token != this)
                            await this.auraManagement(token, this.object, effectFromUUID);
                    }
                }
                // }
            }
        }
    }



}// end TokenDocument

export class ARSTokenLayer extends TokenLayer {
    /**
     * 
     * This override is to push pack versions be dropped into folder called "Map Drops"
     * and to prevent it from making multiple copies there of the same creature (use local if id exists)
     * -cel
     * 
     * The bulk of this is copy/paste from foundry.js otherwise
     * 
     * @param {*} event 
     * @param {*} data 
     * @returns 
     */
    async _onDropActorData(event, data) {
        // console.log("overrides.js _onDropActorData", { event, data });

        // Ensure the user has permission to drop the actor and create a Token
        if (!game.user.can("TOKEN_CREATE")) {
            return ui.notifications.warn(`You do not have permission to create new Tokens!`);
        }

        //** new code -cel
        let actor = await game.actors.get(data.id);
        //** end new code -cel

        // Acquire dropped data and import the actor
        if (!actor) actor = await Actor.implementation.fromDropData(data);
        if (!actor.isOwner) {
            return ui.notifications.warn(`You do not have permission to create a new Token for the ${actor.name} Actor.`);
        }
        if (actor.compendium) {
            const actorData = game.actors.fromCompendium(actor);
            // actor = await Actor.implementation.create(actorData);
            //** new piece -cel
            actor = await Actor.implementation.create({ ...actorData, id: data.id, _id: data.id }, { keepId: true });
            const dumpfolder = await utilitiesManager.getFolder("Map Drops", "Actor");
            // console.log("overrides.js _onDropActorData", { dumpfolder });
            // actor.update({ 'folder': dumpfolder.id });
            // without the delay see .hud errors
            const _timeout = setTimeout(() => actor.update({ 'folder': dumpfolder.id }), 300);
            //** end new piece -cel
        }

        // Prepare the Token data
        const td = await actor.getTokenDocument({ x: data.x, y: data.y, hidden: event.altKey });

        // Bypass snapping
        if (event.shiftKey) td.updateSource({
            x: td.x - (td.width * canvas.grid.w / 2),
            y: td.y - (td.height * canvas.grid.h / 2)
        });

        // Otherwise, snap to the nearest vertex, adjusting for large tokens
        else {
            const hw = canvas.grid.w / 2;
            const hh = canvas.grid.h / 2;
            td.updateSource(canvas.grid.getSnappedPosition(td.x - (td.width * hw), td.y - (td.height * hh)));
        }
        // Validate the final position
        if (!canvas.dimensions.rect.contains(td.x, td.y)) return false;

        // Submit the Token creation request and activate the Tokens layer (if not already active)
        this.activate();
        // const cls = getDocumentClass("Token");
        // return cls.create(td, { parent: canvas.scene });
        return td.constructor.create(td, { parent: canvas.scene });
    }
}

