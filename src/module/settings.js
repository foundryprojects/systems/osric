import { ARS } from './config.js';
import * as debug from "./debug.js"
import { ARSSettingsAudio } from './apps/settings-audio.js'
import { ARSSettingsCombat } from './apps/settings-combat.js'

export const registerSystemSettings = function () {

  game.settings.registerMenu("osric", "arsAudio", {
    name: "Audio",
    label: "Audio Options",      // The text label used in the button
    hint: "Configuration options for ruleset specific audio",
    icon: "fas fa-cog",               // A Font Awesome icon used in the submenu button
    type: ARSSettingsAudio,   // A FormApplication subclass which should be created
    restricted: false                   // Restrict this submenu to gamemaster only?
  });

  game.settings.registerMenu("osric", "arsCombat", {
    name: "Combat",
    label: "Combat Options",
    hint: "Configuration options for ruleset specific combat",
    icon: "fas fa-cog",
    type: ARSSettingsCombat,
    restricted: true,
  });


  // Internal System Migration Version tracking
  game.settings.register("osric", "systemMigrationVersion", {
    name: "System Migration Version",
    scope: "world",
    config: false,
    type: String,
    default: ""
  });

  // Internal System Changelog Version tracking
  game.settings.register("osric", "systemChangeLogVersion", {
    name: "System Changelog Version",
    scope: "world",
    config: false,
    type: String,
    default: ""
  });


  /**
   * 
   * Settings used internally for Party-Tracker
   * 
   */
  game.settings.register("osric", "partyMembers", {
    name: "System Party Tracker - Members",
    scope: "world",
    config: false,
    type: Array,
    default: []
  });
  game.settings.register("osric", "partyAwards", {
    name: "System Party Tracker - Awards",
    scope: "world",
    config: false,
    type: Array,
    default: []
  });
  game.settings.register("osric", "partyLogs", {
    name: "System Party Tracker - Logs",
    requiresReload: false,
    scope: "world",
    config: false,
    type: Array,
    default: []
  });
  // end Party-Tracker settings

  //const osricVariant = game.settings.get("osric", "osricVariant");
  game.settings.register("osric", "osricVariant", {
    name: "SETTINGS.osricVariantLabel",
    hint: "SETTINGS.osricVariantTT",
    scope: "world",
    requiresReload: true,
    config: true,
    type: String,
    choices: {
      "0": "SETTINGS.osricVariant.0",
      "1": "SETTINGS.osricVariant.1",
      "2": "SETTINGS.osricVariant.2"
    },
    default: "0",
    onChange: osricVariant => {
      CONFIG.ARS.settings.osricVariant = osricVariant;
      // change initiative based on variant
      switch (osricVariant) {
        case '0':
          game.settings.set("osric", "initiativeFormula", "1d6");
          // game.settings.settings.get("osric.variant2ArmorDamage").config = false;
          break;
        case '1':
          game.settings.set("osric", "initiativeFormula", "1d6");
          // game.settings.settings.get("osric.variant2ArmorDamage").config = false;
          break;
        case '2':
          game.settings.set("osric", "initiativeFormula", "1d10");
          // game.settings.settings.get("osric.variant2ArmorDamage").config = true;
          break;
        default:
          game.settings.set("osric", "initiativeFormula", "1d6");
          // game.settings.settings.get("osric.variant2ArmorDamage").config = false;
          break;
      }

    }
  });

  //const initiativeUseSpeed = game.settings.get("osric", "initiativeUseSpeed");
  game.settings.register("osric", "initiativeUseSpeed", {
    name: "SETTINGS.initiativeUseSpeed",
    hint: "SETTINGS.initiativeUseSpeedTT",
    scope: "world",
    config: false,
    default: false,
    restricted: true,
    type: Boolean,
  });

  //const initiativeFormula = game.settings.get("osric", "initiativeFormula");
  game.settings.register("osric", "initiativeFormula", {
    name: "SETTINGS.initiativeFormula",
    hint: "SETTINGS.initiativeFormulaTT",
    scope: "world",
    config: false,
    default: "1d6",
    restricted: true,
    type: String,
    onChange: initiativeFormula => CONFIG.Combat.initiative.formula = initiativeFormula
  });

  //const ascendingInitiative = game.settings.get("osric", "InitiativeAscending");
  game.settings.register("osric", "InitiativeAscending", {
    name: "SETTINGS.InitiativeAscending",
    hint: "SETTINGS.InitiativeAscendingTT",
    scope: "world",
    config: false,
    default: true,
    restricted: true,
    type: Boolean,
  });


  //const initSideVSide = game.settings.get("osric", "initSideVSide");
  game.settings.register("osric", "initSideVSide", {
    name: "SETTINGS.initSideVSide",
    hint: "SETTINGS.initSideVSideTT",
    scope: "world",
    config: false,
    default: false,
    restricted: true,
    type: Boolean,
  });


  //const variant2ArmorDamage = game.settings.get("osric", "variant2ArmorDamage");
  game.settings.register("osric", "variant2ArmorDamage", {
    name: "SETTINGS.variant2ArmorDamage",
    hint: "SETTINGS.variant2ArmorDamageTT",
    scope: "world",
    config: false,
    default: false,
    restricted: true,
    type: Boolean,
    onChange: variant2ArmorDamage => CONFIG.ARS.settings.variant2ArmorDamage = variant2ArmorDamage
  });

  //const useAutoHitFailDice = game.settings.get("osric", "useAutoHitFailDice");
  game.settings.register("osric", "useAutoHitFailDice", {
    name: "SETTINGS.useAutoHitFailDice",
    hint: "SETTINGS.useAutoHitFailDiceTT",
    scope: "world",
    config: false,
    default: true,
    restricted: true,
    type: Boolean,
    onChange: useAutoHitFailDice => CONFIG.ARS.settings.autohitfail = useAutoHitFailDice
  });


  //const autoDamage = game.settings.get("osric", "autoDamage");
  game.settings.register("osric", "autoDamage", {
    name: "SETTINGS.autoDamage",
    hint: "SETTINGS.autoDamageTT",
    scope: "world",
    config: false,
    default: false,
    restricted: true,
    type: Boolean,
  });

  //const autoCheck = game.settings.get("osric", "autoCheck");
  game.settings.register("osric", "autoCheck", {
    name: "SETTINGS.autoCheck",
    hint: "SETTINGS.autoCheckTT",
    scope: "world",
    config: false,
    default: false,
    restricted: true,
    type: Boolean,
  });

  //const weaponVarmor = game.settings.get("osric", "weaponVarmor");
  game.settings.register("osric", "weaponVarmor", {
    name: "SETTINGS.weaponVarmor",
    hint: "SETTINGS.weaponVarmorTT",
    scope: "world",
    config: false,
    default: true,
    restricted: true,
    type: Boolean,
  });


  //const rollInitEachRound = game.settings.get("osric", "rollInitEachRound");
  game.settings.register("osric", "rollInitEachRound", {
    name: "SETTINGS.rollInitEachRound",
    hint: "SETTINGS.rollInitEachRoundTT",
    scope: "world",
    config: false,
    default: true,
    restricted: true,
    type: Boolean,
  });


  //const automateLighting = game.settings.get("osric", "automateLighting");
  game.settings.register("osric", "automateLighting", {
    name: "SETTINGS.automateLighting",
    hint: "SETTINGS.automateLightingTT",
    scope: "world",
    config: true,
    default: true,
    restricted: true,
    type: Boolean,
    onChange: automateLighting => {
      CONFIG.ARS.settings.automateLighting = automateLighting
    }
  });

  //const automateVision = game.settings.get("osric", "automateVision");
  game.settings.register("osric", "automateVision", {
    name: "SETTINGS.automateVision",
    hint: "SETTINGS.automateVisionTT",
    scope: "world",
    config: true,
    default: true,
    restricted: true,
    type: Boolean,
    onChange: automateVision => {
      CONFIG.ARS.settings.automateVision = automateVision
    }
  });

  //const initRoundSound = game.settings.get("osric", "initRoundSound");
  game.settings.register("osric", "initRoundSound", {
    name: "SETTINGS.initRoundSound",
    hint: "SETTINGS.initRoundSoundTT",
    scope: "world",
    config: false,
    default: ARS.sounds.initiative.start,
    type: String,
  });
  //const playRoundSound = game.settings.get("osric", "playRoundSound");
  game.settings.register("osric", "playRoundSound", {
    name: "SETTINGS.initRoundSound",
    hint: "SETTINGS.initRoundSoundTT",
    scope: "client",
    config: false,
    default: true,
    type: Boolean,
  });


  //const initTurnSound = game.settings.get("osric", "initTurnSound");
  game.settings.register("osric", "initTurnSound", {
    name: "SETTINGS.initTurnSound",
    hint: "SETTINGS.initTurnSoundTT",
    scope: "world",
    config: false,
    default: ARS.sounds.initiative.turn,
    type: String,
  });
  //const playTurnSound = game.settings.get("osric", "playTurnSound");
  game.settings.register("osric", "playTurnSound", {
    name: "SETTINGS.initTurnSound",
    hint: "SETTINGS.initTurnSoundTT",
    scope: "client",
    config: false,
    default: true,
    type: Boolean,
  });


  //const initVolumeSound = game.settings.get("osric", "initVolumeSound");
  game.settings.register("osric", "initVolumeSound", {
    name: "SETTINGS.initVolumeSound",
    hint: "SETTINGS.initVolumeSoundTT",
    scope: "client",
    config: false,
    range: {
      min: 0,
      max: 1,
      step: 0.1,
    },
    default: 0.5,
    type: Number,
  });


  //const audioPlayTriggers = game.settings.get("osric", "audioPlayTriggers");
  game.settings.register("osric", "audioPlayTriggers", {
    name: "SETTINGS.audioPlayTriggers",
    hint: "SETTINGS.audioPlayTriggersTT",
    scope: "client",
    config: false,
    default: true,
    type: Boolean,
  });

  //const audioTriggersVolume = game.settings.get("osric", "audioTriggersVolume");
  game.settings.register("osric", "audioTriggersVolume", {
    name: "SETTINGS.audioTriggersVolume",
    hint: "SETTINGS.audioTriggersVolumeTT",
    scope: "client",
    config: false,
    range: {
      min: 0,
      max: 1,
      step: 0.1,
    },
    default: 0.5,
    type: Number,
  });

  //const audioTriggerCheckSuccess = game.settings.get("osric", "audioTriggerCheckSuccess");
  game.settings.register("osric", "audioTriggerCheckSuccess", {
    name: "SETTINGS.audioTriggerCheckSuccess",
    hint: "SETTINGS.audioTriggerCheckSuccessTT",
    scope: "client",
    config: false,
    default: ARS.sounds.save.success,
    onChange: audioTriggerCheckSuccess => {
      CONFIG.ARS.sounds.save.success = audioTriggerCheckSuccess
    },
    type: String,
  });
  //const audioTriggerCheckFail = game.settings.get("osric", "audioTriggerCheckFail");
  game.settings.register("osric", "audioTriggerCheckFail", {
    name: "SETTINGS.audioTriggerCheckFail",
    hint: "SETTINGS.audioTriggerCheckFailTT",
    scope: "client",
    config: false,
    default: ARS.sounds.save.failure,
    onChange: audioTriggerCheckFail => {
      CONFIG.ARS.sounds.save.failure = audioTriggerCheckFail
    },
    type: String,
  });
  //const audioTriggerMeleeHit = game.settings.get("osric", "audioTriggerMeleeHit");
  game.settings.register("osric", "audioTriggerMeleeHit", {
    name: "SETTINGS.audioTriggerMeleeHit",
    hint: "SETTINGS.audioTriggerMeleeHitTT",
    scope: "client",
    config: false,
    default: ARS.sounds.combat['melee-hit'],
    onChange: audioTriggerMeleeHit => {
      CONFIG.ARS.sounds.combat['melee-hit'] = audioTriggerMeleeHit
    },
    type: String,
  });
  //const audioTriggerMeleeMiss = game.settings.get("osric", "audioTriggerMeleeMiss");
  game.settings.register("osric", "audioTriggerMeleeMiss", {
    name: "SETTINGS.audioTriggerMeleeMiss",
    hint: "SETTINGS.audioTriggerMeleeMissTT",
    scope: "client",
    config: false,
    default: ARS.sounds.combat['melee-miss'],
    onChange: audioTriggerMeleeMiss => {
      CONFIG.ARS.sounds.combat['melee-miss'] = audioTriggerMeleeMiss
    },
    type: String,
  });
  //const audioTriggerMeleeCrit = game.settings.get("osric", "audioTriggerMeleeCrit");
  game.settings.register("osric", "audioTriggerMeleeCrit", {
    name: "SETTINGS.audioTriggerMeleeCrit",
    hint: "SETTINGS.audioTriggerMeleeCritTT",
    scope: "client",
    config: false,
    default: ARS.sounds.combat['melee-hit-crit'],
    onChange: audioTriggerMeleeCrit => {
      CONFIG.ARS.sounds.combat['melee-hit-crit'] = audioTriggerMeleeCrit
    },
    type: String,
  });
  //const audioTriggerRangeHit = game.settings.get("osric", "audioTriggerRangeHit");
  game.settings.register("osric", "audioTriggerRangeHit", {
    name: "SETTINGS.audioTriggerRangeHit",
    hint: "SETTINGS.audioTriggerRangeHitTT",
    scope: "client",
    config: false,
    default: ARS.sounds.combat['missile-hit'],
    onChange: audioTriggerRangeHit => {
      CONFIG.ARS.sounds.combat['missile-hit'] = audioTriggerRangeHit
    },
    type: String,
  });
  //const audioTriggerRangeMiss = game.settings.get("osric", "audioTriggerRangeMiss");
  game.settings.register("osric", "audioTriggerRangeMiss", {
    name: "SETTINGS.audioTriggerRangeMiss",
    hint: "SETTINGS.audioTriggerRangeMissTT",
    scope: "client",
    config: false,
    default: ARS.sounds.combat['missile-miss'],
    onChange: audioTriggerRangeMiss => {
      CONFIG.ARS.sounds.combat['missile-miss'] = audioTriggerRangeMiss
    },
    type: String,
  });
  //const audioTriggerRangeCrit = game.settings.get("osric", "audioTriggerRangeCrit");
  game.settings.register("osric", "audioTriggerRangeCrit", {
    name: "SETTINGS.audioTriggerRangeCrit",
    hint: "SETTINGS.audioTriggerRangeCritTT",
    scope: "client",
    config: false,
    default: ARS.sounds.combat['missile-hit-crit'],
    onChange: audioTriggerRangeCrit => {
      CONFIG.ARS.sounds.combat['missile-hit-crit'] = audioTriggerRangeCrit
    },
    type: String,
  });
  //const audioTriggerDeath = game.settings.get("osric", "audioTriggerDeath");
  game.settings.register("osric", "audioTriggerDeath", {
    name: "SETTINGS.audioTriggerDeath",
    hint: "SETTINGS.audioTriggerDeathTT",
    scope: "client",
    config: false,
    default: ARS.sounds.combat.death,
    onChange: audioTriggerDeath => {
      CONFIG.ARS.sounds.combat.death = audioTriggerDeath
    },
    type: String,
  });

  //const ctShowOnlyVisible = game.settings.get("osric", "ctShowOnlyVisible");
  game.settings.register("osric", "ctShowOnlyVisible", {
    name: "SETTINGS.ctShowOnlyVisible",
    hint: "SETTINGS.ctShowOnlyVisibleTT",
    scope: "world",
    config: false,
    default: true,
    type: Boolean,
    onChange: ctShowOnlyVisible => {
      CONFIG.ARS.settings.ctShowOnlyVisible = ctShowOnlyVisible
      ui.combat.render();
    }
  });

  //const combatAutomateRangeMods = game.settings.get("osric", "combatAutomateRangeMods");
  game.settings.register("osric", "combatAutomateRangeMods", {
    name: "SETTINGS.combatAutomateRangeMods",
    hint: "SETTINGS.combatAutomateRangeModsTT",
    scope: "world",
    config: false,
    default: true,
    type: Boolean,
  });

  //const floatingHudStaticPosition = game.settings.get("osric", "floatingHudStaticPosition");
  game.settings.register("osric", "floatingHudStaticPosition", {
    name: "SETTINGS.floatingHudStaticPosition",
    hint: "SETTINGS.floatingHudStaticPositionTT",
    scope: "world",
    config: true,
    default: true,
    type: Boolean,
  });

  //const encumbranceIncludeCoin = game.settings.get("osric", "encumbranceIncludeCoin");
  game.settings.register("osric", "encumbranceIncludeCoin", {
    name: "SETTINGS.encumbranceIncludeCoin",
    hint: "SETTINGS.encumbranceIncludeCoinTT",
    scope: "world",
    config: true,
    default: true,
    restricted: true,
    type: Boolean,
    onChange: encumbranceIncludeCoin => {
      CONFIG.ARS.settings.encumbranceIncludeCoin = encumbranceIncludeCoin
    }
  });


  //const identificationActor = game.settings.get("osric", "identificationActor");
  game.settings.register("osric", "identificationActor", {
    name: "SETTINGS.identificationActor",
    hint: "SETTINGS.identificationActorTT",
    scope: "world",
    config: true,
    default: true,
    type: Boolean,
    onChange: identificationActor => {
      CONFIG.ARS.settings.identificationActor = identificationActor
    }
  });

  //const identificationItem = game.settings.get("osric", "identificationItem");
  game.settings.register("osric", "identificationItem", {
    name: "SETTINGS.identificationItem",
    hint: "SETTINGS.identificationItemTT",
    scope: "world",
    config: true,
    default: true,
    type: Boolean,
    onChange: identificationItem => {
      CONFIG.ARS.settings.identificationItem = identificationItem
    }
  });

  //const panOnInitiative = game.settings.get("osric", "panOnInitiative");
  game.settings.register("osric", "panOnInitiative", {
    name: "SETTINGS.panOnInitiative",
    hint: "SETTINGS.panOnInitiativeTT",
    scope: "client",
    config: true,
    default: true,
    restricted: true,
    type: Boolean,
  });


  //const npcNumberedNames = game.settings.get("osric", "npcNumberedNames");
  game.settings.register("osric", "npcNumberedNames", {
    name: "SETTINGS.nPCNumberedName",
    hint: "SETTINGS.nPCNumberedNameTT",
    scope: "world",
    config: true,
    default: true,
    restricted: true,
    type: Boolean,
  });

  //const npcLootable = game.settings.get("osric", "npcLootable");
  game.settings.register("osric", "npcLootable", {
    name: "SETTINGS.npcLootable",
    hint: "SETTINGS.npcLootableTT",
    scope: "world",
    config: true,
    default: true,
    type: Boolean,
    onChange: npcLootable => CONFIG.ARS.settings.npcLootable = npcLootable
  });


  //const itemBrowserMargin = game.settings.get("osric", "itemBrowserMargin");
  game.settings.register("osric", "itemBrowserMargin", {
    name: "SETTINGS.itemBrowserMargin",
    hint: "SETTINGS.itemBrowserMarginTT",
    scope: "world",
    config: true,
    restricted: true,
    range: {
      min: -100,
      max: 100,
      step: 1,
    },
    default: 0,
    type: Number,
    onChange: itemBrowserMargin => {
      if (game.osric.ui?.itembrowser && game.osric.ui.itembrowser.rendered)
        game.osric.ui.itembrowser.render(true);
    }
  });


  //const debugMode = game.settings.get("osric", "debugMode");
  game.settings.register("osric", "debugMode", {
    name: "SETTINGS.debugMode",
    hint: "SETTINGS.debugModeTT",
    scope: "world",
    config: true,
    default: false,
    type: Boolean,
    onChange: debugMode => {
      CONFIG.debug.hooks = debugMode;
      CONFIG.ARS.settings.debugMode = debugMode;
    }
  });




}