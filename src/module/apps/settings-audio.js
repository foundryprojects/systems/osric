import { ARS } from '../config.js';
import * as utilitiesManager from "../utilities.js";
import * as dialogManager from "../dialog.js";
import * as debug from "../debug.js"

export class ARSSettingsAudio extends FormApplication {
    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            id: 'ars-settings-audio-form',
            title: 'Configure Game Settings - Audio',
            template: "systems/osric/templates/apps/settings-audio.hbs",
            width: 600,
            height: 800,
            resizable: true,
            // height: 'auto',
            closeOnSubmit: true,
        });
    }

    async getData() {
        let data = super.getData();
        mergeObject(data, {
            isGM: game.user.isGM,

            initVolumeSound: await game.settings.get("osric", "initVolumeSound"),
            playRoundSound: await game.settings.get("osric", "playRoundSound"),
            initRoundSound: await game.settings.get("osric", "initRoundSound"),
            initTurnSound: await game.settings.get("osric", "initTurnSound"),
            playTurnSound: await game.settings.get("osric", "playTurnSound"),

            audioPlayTriggers: await game.settings.get("osric", "audioPlayTriggers"),
            audioTriggersVolume: await game.settings.get("osric", "audioTriggersVolume"),
            audioTriggerCheckSuccess: await game.settings.get("osric", "audioTriggerCheckSuccess"),
            audioTriggerCheckFail: await game.settings.get("osric", "audioTriggerCheckFail"),
            audioTriggerMeleeHit: await game.settings.get("osric", "audioTriggerMeleeHit"),
            audioTriggerMeleeMiss: await game.settings.get("osric", "audioTriggerMeleeMiss"),
            audioTriggerMeleeCrit: await game.settings.get("osric", "audioTriggerMeleeCrit"),
            audioTriggerRangeHit: await game.settings.get("osric", "audioTriggerRangeHit"),
            audioTriggerRangeMiss: await game.settings.get("osric", "audioTriggerRangeMiss"),
            audioTriggerRangeCrit: await game.settings.get("osric", "audioTriggerRangeCrit"),
            audioTriggerDeath: await game.settings.get("osric", "audioTriggerDeath"),
        });

        return data;
    }

    activateListeners(html) {
        super.activateListeners(html);

        // initRoundSound
        html.find(".file-picker").click(function (event) {
            console.log("", { event })
            const fp = new FilePicker({
                type: "audio",
                wildcard: true,
                current: $(event.currentTarget).prev().val(),
                callback: path => {
                    $(event.currentTarget).prev().val(path);
                }
            });
            return fp.browse();
        });

    }

    async _updateObject(event, formData) {
        // Handle form submission and process the formData
        // console.log("settings-audio.js", { formData })
        for (let setting in formData) {
            // console.log("settings-audio.js", { setting }, ":", formData[setting])
            game.settings.set("osric", setting, formData[setting]);
        }
    }
}